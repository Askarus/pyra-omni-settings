#include "pyrainput_cfg_reader.hpp"
#include "helper_functions.hpp"

#include <sstream>
#include <vector>

Pyrainput_reader::Pyrainput_reader(string path_to_file)
	: path_to_file_{path_to_file}
	{}

void Pyrainput_reader::load_config(){

	using std::ifstream;
	string line;
	ifstream file(path_to_file_);
	while( getline(file, line) ){
		line = helper_functions::trim(line);
		parse_and_store_line(line);
	}
	
	file.close();
	
}

void Pyrainput_reader::parse_and_store_line(string line){
	if (not line_is_valid(line)){return;}
	Line_type line_type;	// different line_types are stored differently because of different number of attributes 
	line_type = determine_line_type(line);
	if(line_type == Line_type::BRIGHTKEY_LINE){
		store_brightkey_line(line);
	}else if(line_type == Line_type::KEY_VALUE_LINE){
		store_key_value_line(line);
	}
}

bool Pyrainput_reader::line_is_valid(string line){
	return line.find("=") != std::string::npos;
}

Line_type Pyrainput_reader::determine_line_type(string line){
	if(line.find(prefix)){
		return Line_type::BRIGHTKEY_LINE;
	}else{
		return Line_type::KEY_VALUE_LINE;
	}
	
}

void Pyrainput_reader::store_brightkey_line(string line){

	using std::string; using helper_functions::trim;
	using std::stringstream;

	// Get left and right Side from "="
	string left_side (line.begin(), (line.begin() + line.find_first_of("=") ) );
	string right_side((line.begin() + line.find_first_of("=") ), line.end() );
	left_side = trim(left_side);
	right_side = trim(right_side);

	//Extract the line attributes
	string button_combination( (left_side.begin() + prefix.length() ) , left_side.end() );
	string device = "";
	string action = "";
	string value = "";
	brightkey_line_extract_standard_attributes(right_side, device, action, value);
	
	Line_brightkey line_read(button_combination, device, action, value);
	brightkey_line_write_initial(line_read);
}

// Converts the string to a stringstream to use getline(stream, target_string, ' ') to get each word.
// Each word is written into a vector. The single arguments will then be written into the arguments passed to the function
void Pyrainput_reader::brightkey_line_extract_standard_attributes(std::string right_side, std::string& device, std::string& action, std::string& value){

	using std::string;
	using std::stringstream;
	stringstream right_side_stream(right_side);
	vector<string> arguments;
    string argument;
    while (getline(right_side_stream, argument, ' ')) {
        arguments.push_back(argument);
    }
    if(arguments.size() >= 1){
		device = arguments[0];
	}
	if(arguments.size() >= 2){
		action = arguments[1];
	}
	if(arguments.size() >= 3){
		value = arguments[2];
	}
	
}

void Pyrainput_reader::brightkey_line_write_initial(Line_brightkey line_read){
	if(line_read.button_combination_ == "normal"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_NORMAL, line_read);
	}else if(line_read.button_combination_ == "Fn"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_FN, line_read);
	}else if(line_read.button_combination_ == "Shift"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_SHIFT, line_read);
	}else if(line_read.button_combination_ == "FnShift"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_FNSHIFT, line_read);
	}else if(line_read.button_combination_ == "Alt"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_ALT, line_read);
	}else if(line_read.button_combination_ == "Ctrl"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_CTRL, line_read);
	}else if(line_read.button_combination_ == "AltCtrl"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_ALTCTRL, line_read);
	}else if(line_read.button_combination_ == "FnAlt"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_FNALT, line_read);
	}else if(line_read.button_combination_ == "FnCtrl"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_FNCTRL, line_read);
	}else if(line_read.button_combination_ == "ShiftAlt"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_SHIFTALT, line_read);
	}else if(line_read.button_combination_ == "ShiftCtrl"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_SHIFTCTRL, line_read);
	}else if(line_read.button_combination_ == "FnShiftAlt"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_FNSHIFTALT, line_read);
	}else if(line_read.button_combination_ == "FnShiftCtrl"){
		brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_FNSHIFTCTRL, line_read);
	}
}

void Pyrainput_reader::brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines which_line, Line_brightkey line_to_check){
	if(line_to_check.device_ != ""){
		lines_brightkey[(int)which_line].set_needs_to_be_written();
		lines_brightkey[(int)which_line].device_ = line_to_check.device_;
		lines_brightkey[(int)which_line].action_ = line_to_check.action_;
		lines_brightkey[(int)which_line].value_ = line_to_check.value_;
	}
}

void Pyrainput_reader::store_key_value_line(string line){
	
}

