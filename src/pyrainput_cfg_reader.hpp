#ifndef PYRAINPUT_CFG_READER
#define PYRAINPUT_CFG_READER

#include "pyrainput_cfg_classes.hpp"
#include "pyrainput_cfg_metadata.hpp"

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <memory>

using std::string;
using std::ifstream;
using std::vector;
using std::unique_ptr;

class Pyrainput_reader{

	string current_line_;
	string path_to_file_;
	void parse_and_store_line(string line);
	bool line_is_valid(string line);
	Line_type determine_line_type(string line);
	void store_brightkey_line(string line);
	void store_key_value_line(string line);
	void brightkey_line_extract_standard_attributes(std::string right_side, std::string& device, std::string& action, std::string& value);
	void brightkey_line_write_initial(Line_brightkey line);
	void brightkey_line_store_if_needed(Pyrainput_cfg_brightkey_lines which_line, Line_brightkey line_to_check);
	
public:
	Pyrainput_reader(string path_to_file);
	void load_config();
	
};

#endif
