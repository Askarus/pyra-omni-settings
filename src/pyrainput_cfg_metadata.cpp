
#include "pyrainput_cfg_metadata.hpp"
#include "pyrainput_cfg_classes.hpp"


std::array<Line_brightkey,BRIGHTKEY_COMBIBATIONS> lines_brightkey{
	Line_brightkey("normal", "screen", "up"),
	Line_brightkey("Fn", "keyboard", "up"),
	Line_brightkey("Shift", "screen", "down"),
	Line_brightkey("FnShift", "keyboard", "down"),
	Line_brightkey("Alt"),
	Line_brightkey("Ctrl"),
	Line_brightkey("AltCtrl"),
	Line_brightkey("FnAlt"),
	Line_brightkey("FnCtrl"),
	Line_brightkey("ShiftAlt"),
	Line_brightkey("ShiftCtrl"),
	Line_brightkey("FnShiftAlt"),
	Line_brightkey("FnShiftCtrl"),
};

std::array<Line_key_value_pair,KEY_VALUE_PAIRS> lines_key_value {
	Line_key_value_pair("mouse.sensitivity", "40"),
	Line_key_value_pair("mouse.deadzone", "20"),
	Line_key_value_pair("mouse.wheel.deadzone", "100"),
	Line_key_value_pair("mouse.click.deadzone", "100"),
	Line_key_value_pair("nubs.deadzone", "10"),
	Line_key_value_pair("nubs.left.x", "mouse_x"),
	Line_key_value_pair("nubs.left.y", "mouse_y"),
	Line_key_value_pair("nubs.right.x", "mouse_btn"),
	Line_key_value_pair("nubs.right.y", "scroll_y"),
	Line_key_value_pair("nubs.left.click", "mouse_left"),
	Line_key_value_pair("nubs.right.click", "mouse_right"),
	Line_key_value_pair("gamepad.export", "1"),
	Line_key_value_pair("keypad.export", "1"),
	Line_key_value_pair("mouse.export", "1"),
};
