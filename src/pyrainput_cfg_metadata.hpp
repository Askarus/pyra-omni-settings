#ifndef PYRAINPUT_CFG_ENUM
#define PYRAINPUT_CFG_ENUM

#include <array>
#include <memory>
#include "pyrainput_cfg_classes.hpp"

constexpr int PYRAINPUT_LINES_TOTAL = 27;
constexpr int BRIGHTKEY_COMBIBATIONS = 13;
constexpr int KEY_VALUE_PAIRS = 14;

const string prefix = "scripts.brightness.";
const string path = "/usr/share/Pyra/scripts/pyra_brightkey.sh";   

// Global arrays containing the pyrainput_fcg lines
extern std::array<Line_brightkey,BRIGHTKEY_COMBIBATIONS> lines_brightkey;
extern std::array<Line_key_value_pair,KEY_VALUE_PAIRS> lines_key_value;

enum class Line_type {
	BRIGHTKEY_LINE = 0,
	KEY_VALUE_LINE,
};

enum class Pyrainput_cfg_brightkey_lines {
	SCRIPTS_BRIGHTNESS_NORMAL = 0,
	SCRIPTS_BRIGHTNESS_FN,
	SCRIPTS_BRIGHTNESS_SHIFT,
	SCRIPTS_BRIGHTNESS_FNSHIFT,
	SCRIPTS_BRIGHTNESS_ALT,
	SCRIPTS_BRIGHTNESS_CTRL,
	SCRIPTS_BRIGHTNESS_ALTCTRL,
	SCRIPTS_BRIGHTNESS_FNALT,
	SCRIPTS_BRIGHTNESS_FNCTRL,
	SCRIPTS_BRIGHTNESS_SHIFTALT,
	SCRIPTS_BRIGHTNESS_SHIFTCTRL,
	SCRIPTS_BRIGHTNESS_FNSHIFTALT,
	SCRIPTS_BRIGHTNESS_FNSHIFTCTRL,
};

enum class Pyrainput_cfg_key_value_lines {
	MOUSE_SENSITIVITY = 0,
	MOUSE_DEADZONE,
	MOUSE_WHEEL_DEADZONE,
	MOUSE_CLICK_DEADZONE,
	NUBS_DEADZONE,
	NUBS_LEFT_X,
	NUBS_LEFT_Y,
	NUBS_RIGHT_X,
	NUBS_RIGHT_Y,
	NUBS_LEFT_CLICK,
	NUBS_RIGHT_CLICK,
	GAMEPAD_EXPORT,
	KEYPAD_EXPORT,
	MOUSE_EXPORT,
};

#endif
