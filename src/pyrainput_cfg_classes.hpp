#ifndef PYRAINPUT_CFG_CLASSES
#define PYRAINPUT_CFG_CLASSES

#include <string>

using std::string;

class LineMaker{

	protected:
	bool needs_to_be_written_ = false;
	
	public:
	virtual string make_line() = 0;
	void set_needs_to_be_written();
	bool needs_to_be_written();
};

class Line_brightkey : public LineMaker {
public:
	string button_combination_;
	string device_;
	string action_;
	string value_;

	bool is_custom_command = false;
	string custom_command = "";
	
public:
	Line_brightkey(string button_combination, string device="", string action="", string value="");
	string make_line();
	void write_device(string device);
	void write_action(string action);
	void write_value(string value);
	
};

class Line_key_value_pair : public LineMaker {
public:
	string key;
	string value;

public:
	Line_key_value_pair(string k, string v="");
	string make_line();
	void set_value(string new_value);
	
};

#endif
