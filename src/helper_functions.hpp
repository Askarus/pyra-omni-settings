#ifndef HELPER_FUNCTIONS
#define HELPER_FUNCTIONS

#include <string>
namespace helper_functions{

std::string trim(std::string const& str);

}
#endif
