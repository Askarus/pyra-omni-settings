#include "pyrainput_cfg_classes.hpp"
#include "pyrainput_cfg_metadata.hpp"
#include "helper_functions.hpp"


//~ const string prefix = "scripts.brightness.";
//~ const string pth = "/usr/share/Pyra/scripts/pyra_brightkey.sh";   



// Line_maker methods
bool LineMaker::needs_to_be_written(){return needs_to_be_written_;};
void LineMaker::set_needs_to_be_written(){needs_to_be_written_ = true;};

// Line_brightkey methods

Line_brightkey::Line_brightkey(string button_combination, string device, string action, string value)
		: button_combination_{button_combination}, device_{device}, action_{action}, value_{value}
	{}

string Line_brightkey::make_line(){
	if(helper_functions::trim(device_) == ""){
		return"";
	}else{
		return helper_functions::trim(prefix + button_combination_ + " = " + device_ + " " + action_ + " " + value_);
	}
}

void Line_brightkey::write_device(string device){
	device_ = device;
}
void Line_brightkey::write_action(string action){
	action_ = action;
}
void Line_brightkey::write_value(string value){
	value_ = value;
}
// Line_simple_key_value_pair methods

Line_key_value_pair::Line_key_value_pair(string k, string v)
		: key{k}, value{v}
	{}

string Line_key_value_pair::make_line(){
	return helper_functions::trim(key + " = " + value);
}

