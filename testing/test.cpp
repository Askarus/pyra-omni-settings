//~ #define BOOST_TEST_MAIN test_main
#include "../src/pyrainput_cfg_classes.hpp"
#define BOOST_TEST_MODULE pyra-omni-settings_test
#include <boost/test/included/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <iostream>

#include "../src/pyrainput_cfg_reader.hpp"
#include "../src/pyrainput_cfg_metadata.hpp"
#include <sstream>
#include <vector>

BOOST_AUTO_TEST_CASE( pyrainput_class_Line_brightkey )
{
    Line_brightkey no_device("combination");
    string line_no_device = no_device.make_line();
    Line_brightkey only_device("combination", "device");
    string line_only_device = only_device.make_line();
    Line_brightkey device_command("combination", "device", "command");
    string line_device_command = device_command.make_line();
    Line_brightkey device_command_value("combination", "device", "command", "value");
    string line_device_command_value = device_command_value.make_line();
    
    BOOST_TEST(line_no_device == "");
    BOOST_TEST(line_only_device == "scripts.brightness.combination = device");
    BOOST_TEST(line_device_command == "scripts.brightness.combination = device command");
    BOOST_TEST(line_device_command_value == "scripts.brightness.combination = device command value");
}

BOOST_AUTO_TEST_CASE ( pyrainput_class_Line_key_value_pair)
{
	Line_key_value_pair no_value("key");
	string line_no_value = no_value.make_line();
	Line_key_value_pair key_value("key", "value");
	string line_key_value = key_value.make_line();

	BOOST_TEST(line_no_value == "key =");
	BOOST_TEST(line_key_value == "key = value");
}

BOOST_AUTO_TEST_CASE ( needs_to_be_written )
{
	Line_brightkey test_line("combination", "device", "command", "value");

    BOOST_TEST(test_line.needs_to_be_written() == false);

    test_line.set_needs_to_be_written();
	BOOST_TEST(test_line.needs_to_be_written() == true);
}

BOOST_AUTO_TEST_CASE ( defaults_pyrainput_cfg_lines_brightkey )
{
	// The 4 first lines do have default values
	BOOST_TEST( lines_brightkey[0].make_line() == "scripts.brightness.normal = screen up");
	BOOST_TEST( lines_brightkey[1].make_line() == "scripts.brightness.Fn = keyboard up");
	BOOST_TEST( lines_brightkey[2].make_line() == "scripts.brightness.Shift = screen down");
	BOOST_TEST( lines_brightkey[3].make_line() == "scripts.brightness.FnShift = keyboard down");

	// The rest should be empty by default
	for(auto line = (lines_brightkey.begin()+4); line!=lines_brightkey.end(); ++line){
		BOOST_TEST( (*line).make_line() == "");
	}
}

BOOST_AUTO_TEST_CASE ( defaults_pyrainput_cfg_lines_key_value_pair ){
	BOOST_TEST( lines_key_value[0].make_line() == "mouse.sensitivity = 40" );
	BOOST_TEST( lines_key_value[1].make_line() == "mouse.deadzone = 20" );
	BOOST_TEST( lines_key_value[2].make_line() == "mouse.wheel.deadzone = 100" );
	BOOST_TEST( lines_key_value[3].make_line() == "mouse.click.deadzone = 100" );
	BOOST_TEST( lines_key_value[4].make_line() == "nubs.deadzone = 10" );
	BOOST_TEST( lines_key_value[5].make_line() == "nubs.left.x = mouse_x" );
	BOOST_TEST( lines_key_value[6].make_line() == "nubs.left.y = mouse_y" );
	BOOST_TEST( lines_key_value[7].make_line() == "nubs.right.x = mouse_btn" );
	BOOST_TEST( lines_key_value[8].make_line() == "nubs.right.y = scroll_y" );
	BOOST_TEST( lines_key_value[9].make_line() == "nubs.left.click = mouse_left" );
	BOOST_TEST( lines_key_value[10].make_line() == "nubs.right.click = mouse_right" );
	BOOST_TEST( lines_key_value[11].make_line() == "gamepad.export = 1" );
	BOOST_TEST( lines_key_value[12].make_line() == "keypad.export = 1" );
	BOOST_TEST( lines_key_value[13].make_line() == "mouse.export = 1" );

}

BOOST_AUTO_TEST_CASE ( test )
{
	Pyrainput_reader reader ("/home/askarus/Pyra-omni-settings++/testing/files/pyrainput.cfg");
	reader.load_config();
	//~ BOOST_TEST(lines_brightkey[(int)Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_NORMAL].make_line() == "scripts.brightness.normal = screen up");
	//~ BOOST_TEST(lines_brightkey[(int)Pyrainput_cfg_brightkey_lines::SCRIPTS_BRIGHTNESS_CTRL].make_line() == "scripts.brightness.Ctrl screen down");
	
}
