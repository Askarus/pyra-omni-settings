pyra-omni-settings:
	g++ src/main.cpp src/pyrainput_cfg_classes.cpp src/pyrainput_cfg_reader.cpp src/pyrainput_cfg_metadata.cpp src/helper_functions.cpp -o pyra-pmni-settings.exec

test:
	g++ src/pyrainput_cfg_classes.cpp testing/test.cpp src/pyrainput_cfg_reader.cpp src/pyrainput_cfg_metadata.cpp src/helper_functions.cpp -o testcase.exec
